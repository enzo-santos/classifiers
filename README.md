# Classifiers

## ManualKNNNotebook

Notebook que realiza a implementação manual da classificação de um 
arquivo de texto por meio do sistema de votação k-Nearest Neighbors.

### Instruções

Para executar o notebook corretamente, é preciso ter os seguintes arquivos 
do repositório, nessa mesma estrutura hierárquica:

- **python/src/util.py**

- **python/src/metrics.py**

- **python/src/preprocessing.py**

- **python/src/classification.py**

## SklearnKNNNotebook

Notebook que realiza a implementação com base na biblioteca `sklearn` da
classificação de um arquivo de texto por meio do sistema de votação
k-Nearest Neighbors.

### Instruções

Para executar o código corretamente, é preciso ter as seguintes bibliotecas instaladas:

- `numpy`, que pode ser instalada por meio do comando

    python -m pip install numpy
    
- `pandas`, que pode ser instalada por meio do comando

    python -m pip install pandas
    
Nota-se que, caso se esteja utilizando o Anaconda, essas bibliotecas já
vem pré-instaladas com o software, não sendo necessária a sua reinstalação.