from . import util

def standard_scale(matrix, ignore_cols=None):
    """
    Padroniza uma matriz.
                                                                        
    Uma matriz pode ser padronizada de tal forma que, para cada elemento 
    de uma coluna, é subtraído a média dos elementos dessa coluna e o
    resultado dividido pelo desvio padrão dos elementos da coluna. Cada
    elemento da matriz resultante será a pontuação padrão da matriz
    original, calculado por (x - mean(X))/stdev(X), onde x é o elemento
    de uma coluna e X são os elementos dessa mesma coluna. Equivalente à
    função 'fit_transform' da classe 'sklearn.preprocessing.StandardScaler'.

    Parâmetros
    ----------
    matrix : list
        Matriz a ser padronizada.

    ignore_cols : opcional, list
        Indíces de colunas a serem ignoradas. Normalmente uma coluna é
        ignorada quando o valor de sua variável é discreta, e não contínua.

    Retorna
    -------
    list
        Matriz padronizada.
    """
    ignore_cols = [] if ignore_cols is None else ignore_cols

    n_rows = len(matrix)
    n_cols = len(matrix[0])
    scaled = [array for array in matrix]
    
    for c in range(n_cols):
        col = [matrix[r][c] for r in range(n_rows)]

        if c not in ignore_cols:
            m = util.mean(col)
            s = util.stdev(col)
            
            for r, elem in enumerate(col):
                scaled[r][c] = (elem - m)/s

        else:
            for r, elem in enumerate(col):
                scaled[r][c] = elem
            
    return scaled