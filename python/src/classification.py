import abc
import math
import sklearn.model_selection as sms

if __name__ == '__main__':
    import util
else:
    from . import util

class Classifier:
    def __init__(self, estimator, hyperparameters=None):
        self._estimator = estimator
        if hyperparameters is None:
            self.estimator = estimator
        else:
            self.estimator = sms.GridSearchCV(estimator,
                hyperparameters, cv=5, iid=False)
        
    def find_best_parameter(self, X, y):
        self.estimator.fit(X, y)
        try:
            self.best_param = self.estimator.best_params_
            return self.best_param
        except:
            return

    def find_accuracy(self, X, y, splits=None):
        cv = 5 if splits is None else splits
        accuracies = sms.cross_val_score(self.estimator, X, y, cv=cv)
        return accuracies.mean(), accuracies.std()

    def __str__(self):
        return self._estimator.__class__.__name__

    def __repr__(self):
        return f'<Classifier estimator={self}>'

class ClassifierComparator:
    def __init__(self, X, y, classifiers=None):
        self.X = X
        self.y = y
        self.classifiers = [] if classifiers is None else classifiers
        
    def add_classifier(self, classifier):
        self.classifiers.append(classifier)

    def compare(self, splits=None):
        classifiers = []
        for classifier in self.classifiers:
            classifier.find_best_parameter(self.X, self.y)
            mean, std = classifier.find_accuracy(self.X, self.y, splits)
            classifiers.append((classifier, mean, std))

        return sorted(classifiers, 
            key=lambda x: math.sqrt(x[1]**2 + (1/x[2] if x[2] else 0)**2), reverse=True)

def split_into_train_test(X, y, test_sampling=0.3):
    """
    Divide amostras em conjuntos aleatórios de treino e de teste.
    
    Equivalente à função 'sklearn.model_selection.train_test_split'.
    
    Parâmetros
    ----------
    X : list
        Matriz representando as características de cada amostra.
        
    y : list
        Vetor representando as classes de cada amostra.
        
    test_sampling : opcional, float
        Proporção do conjunto de dados original a ser incluído nas
        amostras de teste que serão retornadas.

    Retorna
    -------
    tuple
        Conjuntos resultantes da divisão das amostras de entrada.
    """
    list_samples = [(a, c) for a, c in zip(X, y)]
    shuf_samples = util.shuffle(list_samples)
        
    test_size = int(len(shuf_samples)*test_sampling)
    test_samples  = shuf_samples[:test_size]
    train_samples = shuf_samples[test_size:]
        
    X_test_samples  = [x[0] for x in test_samples]
    y_test_samples  = [x[1] for x in test_samples]
    X_train_samples = [x[0] for x in train_samples]
    y_train_samples = [x[1] for x in train_samples]
        
    return X_train_samples, y_train_samples, X_test_samples, y_test_samples

def confusion_matrix(true, pred, n_classes=None, return_classes=False):
    """
    Calcula a matriz de confusão de uma classificação.
    
    Equivalente à função 'sklearn.metrics.confusion_matrix'.
    
    Parâmetros
    ----------
    true : list
        Valores corretos.
        
    pred : list
        Valores predizidos pelo classificador.
        
    return_classes : opcional, bool
        Define se as classes utilizadas pela matriz de confusão
        devem ser retornadas.
        
    Retorna
    ----------
    list/tuple
        A matriz de confusão para analisar a acurácia de uma
        classificação. Caso 'return_classes' seja True, será
        retornada uma tupla com o primeiro valor sendo uma
        lista com as classes utilizadas pela matriz de confusão,
        onde cada classe corresponde a uma coluna.
    """
    classes = list(set(true) | set(pred))
    if n_classes is None:
        n_classes = len(classes)
        
    matrix = []
    for class_ in classes:
        farrays = [t for t in zip(pred, true) if t[1] == class_]
        fpred = [t[0] for t in farrays]
        
        row = [0 for _ in range(n_classes)]
                
        for pvalue in fpred:
            index = classes.index(pvalue)
            row[index] += 1
            
        matrix.append(row)
    
    while len(matrix) < n_classes:
        matrix.append([])

    for row in matrix:
        while len(row) < n_classes:
            row.append(0)

    if return_classes:
        return classes, matrix
    
    return matrix

def kfold_cv(clf, X, y, n_folds=None):
    """
    Utiliza o método de validação cruzada KFold sobre amostras.
    
    Cada fold (folha) é utilizada como conjunto de validação uma vez, enquanto
    as k-1 folds restantes formam o conjunto de treinamento. Equivalente à classe
    'sklearn.model_selection.cross_val_score' com o parâmetro 'cv' igual a 'n_folds'.
    
    Parâmetros
    ----------
    X : list
        Matriz representando as características de cada amostra.
        
    y : list
        Vetor representando as classes de cada amostra.
        
    n_folds : int
        Número de folhas cuja população total será dividida.
    
    Retorna
    ----------
    list
        Vetor das acurácias resultadas da classificação de cada iteração.
    """
    if n_folds is None:
        raise ValueError("'n_folds' must be specified")
        
    list_samples = [(a, c) for a, c in zip(X, y)]
    
    if n_folds > len(list_samples):
        raise ValueError(f"'n_folds' ({n_folds}) must be less or equal than the number of samples ({len(list_samples)})")
    
    list_accuracies = []
    for _ in range(n_folds):
        shuf_samples = util.shuffle(list_samples)
        folds = util.split_into_lists(shuf_samples, n_folds)
        
        folds_out, folds_in = folds[:1], folds[1:]
        
        X_train = []
        y_train = []
        for fold in folds_in:
            for sample in fold:
                array, class_ = sample
                X_train.append(array)
                y_train.append(class_)
        
        X_test = []
        y_test = []
        for fold in folds_out:
            for sample in fold:
                array, class_ = sample
                X_test.append(array)
                y_test.append(class_)
                
        clf.fit(X_train, y_train)
        y_pred = clf.predict(X_test)
        
        cm = confusion_matrix(y_test, y_pred)
        n_test = sum(sum(x) for x in cm)
        accuracy = (cm[0][0] + cm[1][1])/n_test
        
        list_accuracies.append(accuracy)
        
    return list_accuracies
