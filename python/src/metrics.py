def minkowski(v1, v2, p):
    """
    Calcula a distância entre dois vetores pela métrica de Minkowski.
    
    A métrica de Minkowski é considerada a generalização entre as métricas
    de distância Euclidiana e de Manhattan.
    
    Parâmetros
    ----------
    v1 : list
        Primeiro vetor.
        
    v2 : list
        Segundo vetor.
        
    p: int
        Ordem da métrica.

    Retorna
    -------
    float
        Distância entre os dois vetores por Minkowski.
    """
    return sum(abs(e1 - e2)**p for e1, e2 in zip(v1, v2))**(1/p)

def manhattan(v1, v2):
    """
    Calcula a distância entre dois vetores pela métrica de Manhattan.
    
    De acordo com a métrica de Manhattan, a distância entre dois pontos é
    a soma das diferenças absolutas de suas coordenadas cartesianas.
    
    Parâmetros
    ----------
    v1 : list
        Primeiro vetor.
        
    v2 : list
        Segundo vetor.

    Retorna
    -------
    float
        Distância entre os dois vetores por Manhattan.
    """
    return minkowski(v1, v2, 1)

def euclidean(v1, v2):
    """
    Calcula a distância entre dois vetores pela métrica Euclidiana.
    
    De acordo com a métrica de Manhattan, a distância entre dois pontos é
    o comprimento do segmento de reta que os conecta.
    
    Parâmetros
    ----------
    v1 : list
        Primeiro vetor.
        
    v2 : list
        Segundo vetor.

    Retorna
    -------
    float
        Distância Euclidiana entre os dois vetores.
    """
    return minkowski(v1, v2, 2)

def chebyshev(v1, v2):
    """
    Calcula a distância entre dois vetores pela métrica de Chebyshev.
    
    De acordo com a métrica de Chebyshev, a distância entre dois pontos é
    a maior de suas diferenças ao longo de qualquer coordenada.
    
    Parâmetros
    ----------
    v1 : list
        Primeiro vetor.
        
    v2 : list
        Segundo vetor.

    Retorna
    -------
    float
        Distância entre os dois vetores por Chebyshev.
    """
    return max(abs(e1 - e2) for e1, e2 in zip(v1, v2))

def hamming(v1, v2):
    """
    Calcula a distância entre dois vetores pela métrica de Hamming.
    
    De acordo com a métrica de Hamming, a distância entre dois pontos é
    o número de posições nas quais seus elementos correspondentes em
    cada dimensão são diferentes.
    
    Parâmetros
    ----------
    v1 : list
        Primeiro vetor.
        
    v2 : list
        Segundo vetor.

    Retorna
    -------
    float
        Distância entre os dois vetores por Hamming.
    """
    return max(abs(e1 - e2) for e1, e2 in zip(v1, v2))