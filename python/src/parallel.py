import concurrent.futures as cf

def apply_parallel(function, args, n_threads=4):
    pool = cf.ProcessPoolExecutor(n_threads)
    futures = [pool.submit(function, a) for a in args]
    return [f.result() for f in futures]