import abc, math
import os, re, glob
import numpy as np
import pandas as pd
import scipy.stats as stats

class Reader(abc.ABC):
    def __init__(self, root_directory):
        self.path = '/home/enzo/harddrive/GithubProjects/analise-dataset-parkinson/dataset'
        self.users = self.get_users_dict(root_directory)

    def get_users_dict(self, path):
        healthy_pattern = re.compile(r'^controle-(.+?)-(.+?)_')
        parkinson_pattern = re.compile(r'^([A-Z]+)(DIREITA|ESQUERDA)_')
        users = [{}, {}]

        for outcome_folder in os.listdir(path):
            outcome = 1 if outcome_folder == 'parkinson' else 0
            pattern = parkinson_pattern if outcome_folder == 'parkinson' else healthy_pattern
            group_dict = users[outcome]
            
            sensors_path = os.path.join(path, outcome_folder)
            for sensor_folder in os.listdir(sensors_path):
                for file_path in glob.glob(f'{sensors_path}/{sensor_folder}/*.csv'):
                    filename = os.path.split(file_path)[1]
                    
                    search = pattern.search(filename)
                    if not search:
                        continue

                    user_name = search.group(1).lower()
                    user_hand = search.group(2).lower()
                        
                    try:
                        group_dict[user_name]
                        
                    except KeyError:
                        group_dict[user_name] = {}
                        
                    finally:
                        try:
                            group_dict[user_name][sensor_folder]

                        except KeyError:
                            group_dict[user_name][sensor_folder] = {}

                        finally:
                            group_dict[user_name][sensor_folder][user_hand] = file_path

        return users

    @property
    def df(self):
        df = pd.DataFrame()
        
        for outcome, user_dict in enumerate(self.users):
            user_rows = []

            for user_name, sensors in user_dict.items(): 
                for sensor_name, user_hands in sensors.items():
                    for user_hand, user_file in user_hands.items():
                        hand = 'left' if user_hand == 'esquerda' else 'right'
                        sensor = 'acc' if sensor_name == 'acelerometro' else 'gyr'
                        
                        columns = pd.read_csv(user_file, usecols=[3, 4, 5]).to_numpy()
                        columns = np.c_[columns, np.linalg.norm(columns, axis=1)]

                        for i, axis in enumerate(('x', 'y', 'z', 'norm')):
                            column = columns[:, i]

                            for j, values in enumerate(self.transform_data(column)):
                                try:
                                    row = user_rows[j]

                                except IndexError:
                                    row = {'outcome': outcome}
                                    user_rows.append(row)

                                features = self.generate_features(values, outcome)

                                row.update({
                                    f'{sensor} {hand} {axis} {feature}': value
                                    for feature, value in features.items()
                                })
                
                for row in user_rows:
                    df = df.append(row, ignore_index=True, sort=True)

        return df

    @abc.abstractmethod
    def generate_features(self, values, outcome):
        """
        Gera características baseadas em um vetor temporal.

        Será utilizado como callback ao acessar o atributo 'df'.
        Deverá retornar um dicionário com as chaves sendo o nome
        de cada característica e seu respectivo valor.
        """
        pass

    def transform_data(self, values):
        """
        Transforma dados de um vetor temporal em outro tipo de dados.
        """
        yield values