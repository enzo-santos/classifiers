import random
import functools

def show(matrix, sf='{}'):
    """
    Mostra uma matriz de uma forma organizada.
    
    Parâmetros
    ----------
    matrix : list
        Matriz a ser mostrada na tela.
        
    sf : opcional, str
        Formato no qual cada elemento da matriz deve ser mostrado.
    """
    for array in matrix:
        for value in array:
            print(sf.format(value), end=' ')
        print()

def show_X_y(X, y, function_dict=None, format_dict=None,
             uniformize=False, sep='   ', ellipsize=True):
    """
    Mostra um conjunto de amostras de uma forma organizada.
    
    Parâmetros
    ----------
    X : list
        Matriz representando as características de cada amostra.
        
    y : list
        Vetor representando as classes de cada amostra.
        
    function_dict : opcional, dict
        Define se certa função deverá ser aplicada em determinadas
        colunas. A chave do dicionário representa a função a ser
        aplicada e o valor relacionado a cada chave define as colunas
        cuja função será aplicada. O valor pode ser um número inteiro
        (a função será aplicada em apenas uma coluna), um iterável
        de inteiros (a função será aplicada em um conjunto de colunas),
        ou a string 'y', representando a função a ser aplicada nas
        classes de cada amostra.
    
    format_dict : opcional, dict
        Define se certo formato deverá ser aplicado em determinadas
        colunas. A chave do dicionário representa o formato a ser
        aplicado. Caso o valor de uma coluna seja um 'float' e deseja-se
        mostrar apenas três casas decimais, o valor do formato deverá
        ser a string '{:.3f}'. O valor relacionado a cada chave define
        as colunas cujo formato será aplicado. O valor pode ser um número
        inteiro (o formato será aplicado em apenas uma coluna), um iterável
        de inteiros (o formato será aplicado em um conjunto de colunas),
        ou a string 'y', representando o formato a ser aplicado nas
        classes de cada amostra.
    
    uniformize : opcional, bool
        Define se todas as colunas devem ter o mesmo tamanho de linha.
        Caso True, todas as colunas terão o mesmo comprimento da coluna
        mais larga da matriz. Caso contrário, cada coluna terá o comprimento
        do maior valor contido na mesma. O comprimento de uma célula é dado
        pelo espaço que o valor contido nela ocupa, ou seja, uma célula que
        contém o valor '10.4325' (7 caracteres) ocupa mais espaço que uma
        célula de valor '15' (2 caracteres), mesmo o valor da segunda célula
        sendo maior do que o valor da primeira célula.
        
    sep : opcional, str
        Separador a ser utilizado entre os valores das amostras.
    
    ellipsize : opcional, bool/int
        Define se a matriz total deverá ser suprimida após determinado número
        de valores serem mostrados. Caso seu valor seja 'True', serão mostradas
        apenas as primeiras e últimas 5 linhas da matriz. Caso seu valor seja
        'False', toda a matriz será mostrada. Caso seu valor seja 'int', serão
        mostradas as primeiras e últimas linhas definidas pelo valor do parâmetro.
    """
    _print = functools.partial(print, end='')
    _println = functools.partial(print)
    
    def _is_iter(v):
        if type(v) is str:
            return False
        
        try:
            iter(v)
            return True
        
        except TypeError:
            return False
    
    def _get_key(d, v):
        for dk, dv in d.items():
            if _is_iter(dv) and v in dv:
                return dk
            
            if v == dv: return dk
            
        return None
    
    def _filter_value(c):
        fmt_key = _get_key(format_dict, c)
        fmt = str if fmt_key is None else fmt_key.format
        
        fnc_key = _get_key(function_dict, c)
        fnc = (lambda x: x) if fnc_key is None else fnc_key
        
        return lambda x: fmt(fnc(x))
    
    ellipsize = 5 if ellipsize is True else int(ellipsize)
    function_dict = {} if function_dict is None else function_dict
    format_dict = {} if format_dict is None else format_dict
    
    n_rows = len(X)
    n_cols = len(X[0])
    len_id = len(str(n_rows))
    
    if ellipsize:
        rows = [x for x in range(0, ellipsize)] + \
            [x for x in range(n_rows-ellipsize, n_rows)]
        
    else:
        rows = [x for x in range(n_rows)]
        
    len_cols = []
    for c in range(n_cols):
        col = [X[r][c] for r in rows]
        
        filterf = _filter_value(c)                        
        len_col = [len(filterf(x)) for x in col]
        len_cols.append(max(len_col))
    
    max_len_cols = max(len_cols)
    
    for r in rows:            
        _print(f'{r+1:0{len_id}d} [')
        
        _print(sep)
        for c, value, len_col in zip(range(n_cols), X[r], len_cols):
            filterf = _filter_value(c)  
                  
            if uniformize:
                _print(filterf(value).rjust(max_len_cols))
            else:
                _print(filterf(value).rjust(len_col))
                  
            _print('' if c == n_cols-1 else sep)
            
        _print('], y = ')
        
        filterf = _filter_value('y')  
        _println(filterf(y[r]))
                         
        if r == ellipsize-1:
            _println('...')

def mean(array):
    """
    Calcula a média de um conjunto de valores.
    
    Parâmetros
    ----------
    array : list
        Lista cuja média será calculada.
        
    Retorna
    ----------
    float
        Média da lista.
    """
    return sum(array)/len(array)

def stdev(array, ddof=0):
    """
    Calcula o desvio padrão de um conjunto de valores.
    
    Parâmetros
    ----------
    array : list
        Lista cujo desvio padrão será calculado.
        
    ddof : opcional, int
        Grau de liberdade da lista.
        
    Retorna
    ----------
    float
        Desvio padrão da lista.
    """
    m = mean(array)
    s = sum((x - m)**2 for x in array)
    return (s/(len(array) - ddof))**0.5

def shuffle(array):
    """
    Embaralha um conjunto de valores.
    
    É utilizado o método de embaralhamento Fisher-Yates.
    
    Parâmetros
    ----------
    array : list
        Lista a ser embaralhada.
        
    Retorna
    ----------
    list
        Lista embaralhada.
    """
    a = [x for x in array]
    
    for i in range(len(a)-1, 0, -1):
        j = random.randint(0, i)
        a[i], a[j] = a[j], a[i]
        
    return a    
                  
def diag_sum(matrix):
    """
    Calcula a soma da diagonal principal de uma matriz.
    
    Parâmetros
    ----------
    matrix : list
        Matriz quadrada cuja soma da diagonal será calculada.
        
    Retorna
    ----------
    float
        Soma da diagonal principal.
    """
    if any(len(row) != len(matrix[0]) for row in matrix):
        raise ValueError('all the rows of the matrix must be the same length')
                  
    if len(matrix) != len(matrix[0]):
        raise ValueError('matrix must be square')
                  
    sum_ = 0
    for i, row in enumerate(matrix):
        sum_ += row[i]
    
    return sum_
                  
def split_into_lists(array, n_lists):
    """
    Divide uma lista em um conjunto de listas.
    
    Equivalente à função 'numpy.array_split'.
    
    Parâmetros
    ----------
    array : list
        Lista a ser dividida.
        
    n_lists : int
        Número de listas geradas a partir da lista original.
        
    Retorna
    ----------
    list
        Conjunto de listas menores com elementos da lista original.
    """
    k, m = divmod(len(array), n_lists)
    
    if n_lists > len(array):
        raise ValueError(f"'n_lists' ({n_lists}) must be less \
                or equal than the length of the array ({len(array)})")
    
    lists = []
    for i in range(n_lists):
        p0 = i * k + min(i, m)
        p1 = (i+1) * k + min(i+1, m)
        lists.append(array[p0:p1])
        
    return lists

def sort(a1, a2, reverse=False):
    """
    Organiza uma lista baseada nos valores ordenados de outra lista.
    
    Parâmetros
    ----------
    a1 : list
        Lista cujos valores serão ordenados.
        
    a2 : list
        Lista cujos valores serão organizados com base na ordenação de 'a1'.
        
    Retorna
    ----------
    list
        Lista 'a2' com os valores organizados baseado em 'a1'.
    """
    sorted_zip = sorted(zip(a1, a2), reverse=reverse, key=lambda t: t[0])
    return [t[1] for t in sorted_zip]

def load_csv(path, y_col, X_cols=None, sep=',', skip_rows=0, return_header=False):
    """
    Carrega amostras de um arquivo CSV.
    
    Parâmetros
    ----------
    path : str
        Diretório do arquivo.
        
    y_col : int/str
        Coluna onde estão as classes de cada amostra. Caso seja 'str',
        o valor da sua coluna será definido pelo cabeçalho do arquivo.
        
    X_cols : opcional, iterável
        Colunas onde estão as características de cada amostra. Caso
        não seja passado como parâmetro, todas as colunas exceto a
        coluna 'y_col' serão utilizadas como característica.
        
    sep : opcional, str
        Separador de valores utilizado pelo arquivo. Caso não seja
        passado como parâmetro, o separador utilizado será a vírgula.

    skip_rows : opcional, int
        Número de linhas a ser ignoradas do começo do arquivo. Caso
        não seja passado como parâmetro, nenhuma linha será ignorada.
        
    return_header : opcional, bool
        Define se o cabeçalho do arquivo deverá ser retornado.
        
    Retorna
    ----------
    tuple
        A primeira posição da tupla será a matriz de características de
        cada amostra e a segunda posição da tupla será o vetor de classes
        de cada amostra. Caso 'return_header' == True, a terceira posição
        da tupla será o cabeçalho do arquivo em formato de lista.
    """
    X = []
    y = []
    
    with open(path) as f:
        for i in range(skip_rows):
            if i == 0:
                line = f.readline().strip().split(sep)
                
                if return_header:
                    header = line
                
                if type(y_col) is str:
                    y_col = line.index(y_col)
                
                if X_cols is not None:
                    X_cols = [line.index(col) if type(col)
                        is str else col for col in X_cols]
                
            else:
                f.readline()
            
        for line in f:
            row = []
            str_data = line.strip().split(sep)
            data = [float(value) if value else 0 for value in str_data]
            
            for i, value in enumerate(data):
                if X_cols is not None and i not in X_cols:
                    continue
                    
                if i == y_col:
                    continue
                    
                row.append(value)
                
            X.append(row)
            y.append(data[y_col])
    
    if not return_header:
        return X, y
    
    return X, y, header

