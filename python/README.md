# Configurando o ambiente de trabalho

## Requisitos

- Baixar a versão mais recente do Python ([site oficial](https://www.python.org/downloads/))

- **(Opcional)** Baixar a versão mais recente do Sublime Text ([site oficial](https://www.sublimetext.com/3))
ou outro editor de texto 

## Baixando o repositório do kNN

Entre no link do repositório no GitHub (https://github.com/EnzoGabriel29/classifiers).

Em **Clone or download**, clique em **Download ZIP**.

![](http://i.imgur.com/PdrcnOc.png)

Após extrair o arquivo .zip para a sua máquina, vá para o diretório onde os arquivos foram
extraídos e, utilizando a IDLE do Python (
[instruções](https://civcomunb.wordpress.com/2016/07/11/3-formas-de-rodar-python/),
seção 1) ou algum editor de texto (como o Sublime Text), abra o arquivo

    DIRETORIO_ARQUIVOS_EXTRAIDOS/python/src/knn/main.py

Lá, é possível ter acesso às funções de outros módulos e alterar o seu conteúdo dependendo
do arquivo que deseja ser analisado. As documentações dos códigos estão nos arquivos Python,
tanto na pasta `/python/src` quanto na pasta `/python/src/knn`.